\subsubsection{Algorithmus von Overmars und Welzl}
\label{subsubsec:ow-algo}

Wie auch der Algorithmus von Lee ziehlt der verbesserte Algorithmus von Mark Overmars und Emo Welzl\footnote{\linespread{1.0}\selectfont{}In ihrer Arbeit \cite{overmars-welzl} stellen Overmars und Welzl zwei ähnliche Algorithmen vor.
In diesem Abschnitt wird auf den ,,\hspace{0pt}ersten`` Algorithmus mit quadratischer Laufzeit $O(n^2)$ Bezug genommen.}, die diesen 1988 vorgestellt haben, auf eine verbesserte Verarbeitung der Kanten ab.
Mit dem nachfolgend beschriebenen Algorithmus, der vergleichsweise simpel zu implementieren ist\cite[S.~1]{overmars-welzl}, lassen sich Visibility Graphs ebenfalls berechnen.
In der Praxis müssen einige Änderungen vorgenommen werden, um auch mit Polygonen korrekt umgehen zu können, weil die originale Beschreibung des Algorithmus auf sich nicht schneidenden Strecken aufsetzt\cite[S.~12]{kitzinger}.

Für den Algorithmus muss das Konzept des \emph{Rotation Tree} verstanden werden.
Dabei handelt es sich um einen planaren Baum\cite[S.~2f]{rooted-trees}, der ähnlich wie ein binärer Suchbaum strukturiert, aber nicht auf zwei Kinder ($k=2$) beschränkt ist.
Vielmehr lassen sich pro Knoten beliebig viele Kinder speichern ($k\leq{}n$).
Aufgrund seiner Eigenschaft als planarer Graph können in einem \emph{Rotation Tree} niemals sich kreuzende Kanten auftreten.
Dies hilft insbesondere beim Erstellen von Repräsentationen oder Nachvollziehen der Operationen, die auf den Baum angewendet werden, weiter.

\begin{figure}[!ht]
	\centering
	\vspace{-10pt}
	\includegraphics[keepaspectratio,width=0.95\textwidth]{img/OW-BasicExample}
	\vspace{-14pt}
	\caption{Beispiel eines neuen und eines fertigen \emph{Rotation Tree} (selbst erstellt)}
	\vspace{-13pt}
	\label{fig:ow-basic-example}
\end{figure}

\noindent
Als ,,echte Knoten`` werden im Folgenden alle Elemente des Baums bezeichnet, die im ursprünglichen Graphen als Punkte zu finden sind.
Denn zusätzlich gibt es in einem \emph{Rotation Tree} auch die beiden Knoten $-\infty$ und $+\infty$.
Beide sind minimal weiter rechts als alle anderen echten Knoten der Hindernisse, was einer etwas größeren x-Koordinate entspricht.
Zudem befindet sich $-\infty$ unendlich weit unterhalb (kleinste y-Koordinate) und $+\infty$ unendlich weit oberhalb (größte y-Koordinate) aller anderen Knoten.
Wie in Abbildung~\ref{fig:ow-basic-example} erkennbar, zeigen anfangs alle echten Knoten auf $-\infty$, der wiederum auf $+\infty$ zeigt\cite[S.~4]{vg-3d}.
Zu jedem Zeitpunkt stellt $+\infty$ die Wurzel des Baums dar.

Jedes Element des Baums speichert Referenzen auf vier andere Elemente, sofern diese vorhanden sind: den am weitesten rechts liegenden Kindsknoten (\emph{rightmost}), das \mbox{Elternelement} (\emph{parent}) sowie die beiden Geschwister rechts und links (\emph{siblings}) \cite[S.~10]{kitzinger}.
Neben dem \emph{Rotation Tree} wird auch ein gewöhnlicher \emph{Stack} (Stapel) verwaltet, der \mbox{alle} linken Blätter des Baumes (Knoten ohne Kindsknoten) enthält, die noch verarbeitet \mbox{werden} müssen.
Verglichen mit alternativen Strategien wie \emph{Priority Queues} reduziert dies die Komplexität einer Implementierung und beschleunigt das Verfahren\cite[S.~2]{overmars-welzl}.

Im Grunde wird für jeden Knoten analog zum Ansatz von Lee eine Scanline in \mbox{Richtung} $d$ mit $\SI{-90}{\degree}\leq{}d\leq{}\SI{90}{\degree}$ verwendet.
Dabei entspricht $d=\SI{0}{\degree}$ einer \mbox{Parallelen} zur x-Achse.
Im Gegensatz zum vorherigen Verfahren wird diese Scanline allerdings nicht sequentiell für alle Knoten durchgeführt, sondern simultan durch die Verwendung des \emph{Rotation Tree} in Verbindung mit dem Stapel koordiniert.
Analog zur anderen Variante wird auch hier jeweils von Knoten zu Knoten gesprungen und nicht etwa kontinuierlich anhand eines Winkels rotiert.

\begin{figure}[!ht]
	\centering
	\fbox{
		\includegraphics[height=97pt,width=0.45\textwidth]{img/OW-Reattach-Grandparent}
	}
	\fbox{
		\includegraphics[height=97pt,width=0.45\textwidth]{img/OW-Reattach-Chain}
	}
	\vspace{-4pt}
	\caption{Beispiel der Möglichkeiten zum Verschieben eines Knotens (selbst erstellt)}
	\vspace{-14pt}
	\label{fig:ow-reattach-methods}
\end{figure}

\noindent
Im Folgenden wird der Algorithmus auf den \emph{Rotation Tree} anhand des Pseudocodes in Abbildung~\ref{alg:ow-main} auf \mbox{Seite~\pageref{alg:ow-main}} erklärt.
Ein Beispiel zur schrittweisen Verarbeitung des Hindernisses findet sich im Anhang in Darstellung~\ref{fig:ow-tree-processing} auf Seite~\pageref{fig:ow-tree-processing}.

Zuerst werden alle echten Knoten nach fallender x-Koordinate sortiert.
\mbox{Anschließend} wird der grundlegende \emph{Rotation Tree} aufgebaut, indem die Knoten gemäß der \mbox{Sortierung} stets als rechte Kindsknoten von $-\infty$ eingefügt werden.
Dann wird der \emph{Stack} mit dem Knoten mit der größten x-Koordinate initialisiert.
Die folgende Hauptschleife läuft so lange noch Elemente auf dem Stapel liegen.

In der Schleife wird stets ein Knoten $p$ vom Stapel genommen und seine Position und Verhältnisse zu den anderen Elementen bestimmt.
Der Elternknoten wird nachfolgend mit $q$ bezeichnet.
Falls $q\not=-\infty$ ist, wird die Kante $\big[\,pq\,\big]$ auf Sichtbarkeit geprüft.
Dann wird $p$ im Baum verschoben.
Entweder wird er als linker Bruder von $q$ wie im linken Beispiel (Fall 1) oder wie im rechten Beispiel als rechtes Kind zur darüber liegenden Reihe hinzugefügt (Fall 2).
Die Reihe ergibt sich aus der Überlegung der Scanline, die alle Knoten gegen den Uhrzeigersinn prüft, und der Eigenschaft des planaren Graphen.
Denn es dürfen keine Überschneidungen auftreten.
Am Ende wird $p$ wieder auf den Stapel gelegt, wenn er an der neuen Position keinen linken Bruder hat und $q\not=+\infty$ ist.
Zudem wird der alte rechte Bruder ebenfalls auf den Stapel gelegt, sofern er existiert.

Die Sichtbarkeitsprüfung erfolgt bei diesem Algorithmus nicht durch die Prüfung auf die Wurzel des Baums wie beim Ansatz von Lee sondern den Vergleich von Kanten.
Dazu wird für jeden Knoten die Kante gespeichert, die im vorherigen Verarbeitungsschritt sichbar war.
Gibt es keine solche Kante, ist sie einfach \texttt{null}.
Zu Beginn der Prozedur wird ebenfalls eine Form des Scans durchgeführt, um den Anfangsstand zu erkennen.
Dabei rotiert diese Scanline allerdings nicht, sondern verläuft geradewegs nach $\SI{-90}{\degree}$.
Eine Sichtlinie besteht zwischen den zu prüfenden Knoten $p$ und $q$ nur dann, wenn eine der drei folgenden Bedingungen erfüllt ist.
Entweder ist $\big[\,pq\,\big]$ bereits Teil des ursprünglichen Graphen oder die mit $q$ assoziierte Kante ist die vorherige Kante von $p$.
Alternativ kann der Abstand $\overline{pq}$ zwischen den beiden Knoten auch kleiner sein als der Abstand zwischen $p$ und der vorher sichtbaren Kante.
Nur dann, wenn eine Umsortierung im \emph{Rotation Tree} erfolgt und Sichtbarkeit festgestellt worden ist, wird die vorherige Kante geändert.
Auf diese Weise muss auch keine Liste von ,,of{}fenen Kanten`` verwaltet werden.

Für weitergehende Informationen und spezifische Details für eine Implementierung sei auf das Werk von Overmars und Welzl \cite{overmars-welzl} verwiesen.

Die Laufzeit des Algorithmus steigt quadratisch mit der Anzahl der echten Knoten, da es grundsätzlich auch die Prüfung aller Kanten untereinander gibt.
Die Operationen auf den Stapel und die Aktionen im Baum sind jeweils in konstanter Laufzeit möglich, weshalb es den logarithmischen Faktor wie beim Algorithmus von Lee nicht gibt.
Daher wird die Laufzeitkomplexität insgesamt mit $O(n^2)$ angegeben\cite[S.~12]{kitzinger}.

Die Platzkomplexität ist gemäß der Landau-Notation\cite{landau} mit $O(n)$ anzugeben.
Denn es wird sowohl der Graph bzw. \emph{Rotation Tree} mit $n$ Elementen als auch der \emph{Stack}, der maximal $n$ Elemente enthält, gespeichert.
Damit hängt der Speicherbedarf linear von der Anzahl der Knoten ab.
