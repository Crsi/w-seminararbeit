\subsubsection{Algorithmus von Lee}
\label{subsubsec:lee-algo}

Da der naive Ansatz of{}fenbar nicht ideal ist, wird nun anhand des nicht-trivialen, nach D.~T.~Lee benannten Algorithmus erklärt, wie eine bessere Problemlösung aussehen kann.
Die Grundidee hierbei ist eine ef{}fektivere Verarbeitung der Kanten des Graphen.

Auch wenn sich die Details der Implementierung in der Literatur minimal voneinander unterscheiden\cite[S.~7]{kitzinger}\cite{lee-ileana}, sind die Hauptmerkmale stets die Gleichen.
Die Knoten aller Polygone werden anhand des Winkels zu einem Punkt $p$ sortiert.
Dazu rotiert ein in positiver x-Richtung beginnender Strahl $S$ -- die \emph{Scanline} -- im Uhrzeigersinn einmal über alle anderen Knoten um den Punkt $p$ herum.
Diese werden im Beispiel in Abbildung \ref{fig:example-lee-scan} aufsteigend mit $w_1$ bis $w_7$, die Kanten dazwischen entsprechend mit $e_1$ bis $e_7$, bezeichnet.

Es wird eine Datenstruktur (meistens ein \emph{Self-Balancing Binary Search Tree}) der ,,of{}fenen Kanten`` verwaltet.
Als Schlüssel für den Baum dient der Abstand zwischen dem aktuell verarbeiteten Knoten $p$ und dem Schnittpunkt der Scanline mit der Kante.
Da die Kanten der Hindernisse sich allerdings nur in Knoten schneiden und sich damit auch nur in Knoten Änderungen in der Reihenfolge der hintereinander liegenden Kanten ergeben, wird der Abstand zwischen $p$ und $w_x$ als Schlüssel verwendet.

\begin{figure}[!t]
	\vspace{-8pt}
	\centering
	\includegraphics[keepaspectratio,width=\textwidth]{img/Lee-Alg_example-base}
	\vspace{-28pt}
	\caption{Beispiel einer Scanline über zwei Polygone (selbst erstellt)}
	\label{fig:example-lee-scan}
	\vspace{-14pt}
\end{figure}

\noindent
Der Baum enthält anfangs bereits alle nach Abstand sortierten Kanten der Polygone, die beim Starten der Scanline Schnittpunkte mit dieser haben.
In der folgenden Erklärung wird zur besseren Darstellung statt eines Baums allerdings eine sortierte Liste wie in Abbildung~\ref{table:open-edges} verwendet, die sich aus dem Beispiel von Abbildung~\ref{fig:example-lee-scan} ergibt.

\begin{wrapfigure}{l}{0.37\textwidth}
	\centering
	\vspace{-8pt}
	\begin{tabular}{c|c}
		\textbf{Schritt} & \textbf{Of{}fene Kanten}\\
		\hline
		0 & $[\mbox{ }]$\\
		1 & $[e_2, e_1]$\\
		2 & $[e_2, e_3]$\\
		3 & $[e_5, e_4, e_2, e_3]$\\
		4 & $[e_5, e_4]$\\
		5 & $[e_5, e_6]$\\
		6 & $[e_7, e_6]$\\
		7 & $[\mbox{ }]$\\
	\end{tabular}
	\caption{Beispiel der Verarbeitung aller Knoten}
	\vspace{-20pt}
	\label{table:open-edges}
\end{wrapfigure}

Der Algorithmus beginnt mit einer leeren Liste, da es anfangs keine Kanten gibt, die Schnittpunkte mit der Scanline haben.
Anschließend beginnt der Kern des Algorithmus.
Beim Verarbeiten des Knotens $w_1$ fügt er die beiden Kanten $e_1$ und $e_2$ hinzu.
Das \mbox{Gleiche} geschieht beim Knoten $w_3$ für die Kanten $e_4$ und $e_5$ und stellt den ersten von drei möglichen Fällen, die in Abbildung~\ref{fig:lee-scanline-cases} aufgezeigt werden, dar.
Diese erste Wahl wird bei Knoten getroffen, die von keiner offenen (im Baum gespeicherten) Kante berührt werden.
Andersherum verhält es sich bei $w_4$ und $w_7$.
In diesem Fall werden die Kanten $e_2$ und $e_3$ sowie $e_6$ und $e_7$ aus dem Baum entfernt, da beide Kantenpaare jeweils bereits in der Datenstruktur gespeichert waren.
Bei der dritten Möglichkeit wird in einem Schritt sowohl eine Kante entfernt als auch eine neue Kante hinzugefügt.
Dieses Verhalten tritt bei den Knoten $w_2$, $w_5$ und $w_6$ auf, da hier eine Kante of{\hspace{0pt}}fen und die andere Kante noch nicht behandelt worden ist.
Es wird für $w_5$ demnach $e_4$ entfernt und $e_6$ hinzugefügt.

Die Reihenfolge der Kanten in der Liste ist wichtig, da das erste Element der Liste bzw. das Wurzelelement des Baums stets das einzig sichtbare Element von $p$ für einen bestimmten Winkel $\alpha$ ist.
Daher müssen hintereinander liegende, kollineare Punkte auch speziell behandelt werden, weil der Algorithmus ansonsten davon ausgehen würde, dass alle hinteren Punkte verdeckt wären.

Beim Verarbeiten von $w_4$ ist dieser Knoten beispielsweise nicht sichtbar, da vor den beiden zu entfernenden of{}fenen Kanten $e_2$ und $e_3$ noch die of{}fenen Kanten $e_4$ und $e_5$ existieren.
In der Repräsentation als Baum wäre $e_5$ das Wurzelelement, weshalb es für den Winkel zu $w_4$ die erste (und einzige) sichtbare Kante ist.
Dieses simple Verfahren, die Sichtbarkeit zwischen $p$ und einem anderen Knoten $w_x$ zu bestimmen, macht den Algorithmus insgesamt schneller als den naiven Ansatz, bei dem die Berechnung der Sichtbarkeit über das Finden von Schnittpunkten aller Kanten untereinander erfolgt.

\begin{wrapfigure}{r}{0.42\textwidth}
	\centering
	\vspace{-14pt}
	\includegraphics[keepaspectratio,width=0.4\textwidth]{img/Lee-Scanline_methods}
	\vspace{-2pt}
	\caption{Beispiel mit den drei Fällen (selbst erstellt)}
	\vspace{-20pt}
	\label{fig:lee-scanline-cases}
\end{wrapfigure}

Die Scanline muss für alle $n$ Knoten des \mbox{Graphen} berechnet werden.
Dabei überstreicht sie alle anderen $n-1$ Knoten.
Dies allein resultiert in einer Zeitkomplexität von $O(n^2)$.
Da die Zeit für die Operationen zum Einfügen und Entfernen von Elementen logarithmisch von der Größe des Baums abhängt, der maximal $n$ Elemente enthält, wird die dafür benötigte Zeit mit $O(\log{n})$ angegeben.
Insgesamt beläuft sich die Laufzeit des Scanline-Algorithmus daher auf $O(n^2\log{n})$.

Teil des Algorithmus ist auch die Sortierung der Knoten nach ihrem Winkel zu einem \mbox{Zentrum} $p$, was jeweils in einer Zeit von $O(n)$ erledigt \mbox{werden} kann.
Für alle $n$ Elemente ergibt sich \mbox{hierfür} eine Zeitkomplexität von $O(n^2)$.
Die Sortierung kann bei der Angabe der \mbox{oberen} Schranke für den gesamten Algorithmus von Lee jedoch vernachlässigt werden\cite{landau}, da für $n\to\infty$ gilt: $O(n^2\log{n}) > O(n^2)$.
Nähere Details f{}inden sich unter anderem in \cite[S.~3]{visibility-graph}.
