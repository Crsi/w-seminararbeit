\subsection{Grundlagen}

\subsubsection{Grundidee der Problemlösung}

Es ist ein möglichst guter Pfad\footnote{In der Aufgabe ist der kürzeste Weg nicht der beste Weg. Aber das ist für diese Arbeit nicht relevant.} gesucht, dem Lisa über das Feld folgen kann.
Daher wird zuerst eine Art von Karte erstellt, die die Umgebung von Lisa modelliert.
Auf dieser Karte wird anschließend eine Pfadsuche durchgeführt, um die möglichst günstige Route zur Straße zu erhalten, sodass sie den Bus noch erreichen kann.

Auch wenn es andere Ansätze wie \emph{Rapidly-Exploring Random Trees}\cite{rrt-basic} und diverse Erweiterungen und Verbesserungen wie \emph{RRT*-Smart}\cite{rrt*-smart} gibt, die für die Pfadsuche in einem ähnlichem Umfeld ebenfalls sehr geeignet sind, wird zur Lösung ein \emph{Visibility Graph} (VG) verwendet.
Ein Vorteil eines solchen Graphen ist beispielsweise, dass nach der Generierung beliebige Manipulationen oder weiterführende Berechnungen möglich sind.
So enthält ein VG beispielsweise bereits die konvexen Hüllen aller Hindernisse.
Man kann einen Visibility Graph häufig als eine Art Landkarte verstehen, die aus begehbaren, freien Wegen besteht.
Auf dieser Landkarte wird anschließend ein Weg vom Start zum Ziel gesucht.
Da in der Praxis der Großteil des Rechenaufwandes in die Erstellung des Visibility Graph f{}ließt\footnote{Siehe Abschnitt \ref{subsubsec:solution-task}}, wird in dieser Arbeit besonders darauf Bezug genommen.

\subsubsection{Begrif{}fsdef{}initionen}

\paragraph{Hindernis} Ein Hindernis ist ein beliebiges, simples Polygon.
Ein solches Polygon hat eine überschneidungsfreie Innenf{}läche ohne Löcher und genau so viele Ecken wie Kanten, wobei stets genau zwei Kanten an einer Ecke aufeinander tref{}fen.

\paragraph{Sichtbarkeit} Zwei Punkte $P$ und $Q$, die sich nicht innerhalb eines Hindernisses befinden, sind voneinander \emph{sichtbar}, wenn ihre Verbindungsstrecke (Sichtlinie) $[PQ]$ keine Hindernisse schneidet\footnote{Eine Kante eines Polygons ist eine Sichtlinie zwischen den beiden Ecken, die die Kante einschließen.}.
Wenn die Sichtlinie ein Hindernis in einem Eckpunkt berührt, ist die Sichtbarkeit nicht beeinträchtigt, da dabei kollineare Strecken\cite[S.~4f]{kitzinger} \mbox{entstehen}.
Laut einer anderen, äquivalenten Def{}inition sind zwei Punkte $P$ und $Q$ voneinander \emph{sichtbar}, wenn kein Punkt $p\in{}[PQ]$ Teil der Innenf{}läche eines Polygons ist.

\subsubsection{Visibility Graph}

Ein Visibility Graph (VG) ist ein meist ungerichteter Graph, dessen Knoten (\emph{vertices},~$V$) je nach Def{}inition die sichtbaren oder freien Punkte sind, die durch Sichtlinien (\emph{edges},~$E$) miteinander verbunden sind.
Zwischen zwei Knoten eines VG gibt es demnach eine Kante, wenn es eine Sichtlinie zwischen den beiden abgebildeten Punkten gibt und vice versa.

\begin{figure}[!ht]
	\centering
	\includegraphics[keepaspectratio,width=0.75\textwidth]{img/VisGraph_full-reduced}
	\caption{Beispiel zweier Visibility Graphs (selbst erstellt)}
	\label{fig:basic.complete-reduced-vg}
	\vspace{-16pt}
\end{figure}

\noindent
Wie in Abbildung~\ref{fig:basic.complete-reduced-vg} zu erkennen ist, wird zwischen einem vollständigen VG links und einem reduzierten VG rechts unterschieden.
In der Abbildung sind zwischen den Ecken der zwei Hindernisse -- sofern möglich -- Sichtlinien eingezeichnet.
Der rechte, reduzierte VG geht aus dem vollständigen VG hervor, indem alle Kanten ignoriert werden, die für die spätere Pfadsuche irrelevant sind.
Relevant sind alle Kanten, die einen beliebigen Pfad durch die äußeren Knoten um Hindernisse herum führen\cite[S.~10]{visgraph}.

\subsubsection{Anwendung zur Problemlösung}
\label{subsubsec:basic.application}

Für die Aufgabe des Bundeswettbewerbs musste auf dem bereits erstellten Visibility Graph das SSSP-Problem (\emph{Single Source Shortest Path}) in modif{}izierter Form gelöst werden.
Dazu können beispielsweise die nicht optimale Breitensuche\cite[S.~341--343]{intro-it}, der Algorithmus von Dijkstra, der A*-Algorithmus oder der Bellman-Ford-Algorithmus\cite{shortest-path-algs} verwendet werden.

% Formatting
\makenewpage

\subsubsection{Optimalität}
\label{subsubsec:basic.optimal}

\begin{wrapfigure}{l}{0.45\textwidth}
	\centering
	\vspace{-12pt}
	\includegraphics[keepaspectratio,width=0.44\textwidth]{img/VG_proof_shortest-path}
	\vspace{7pt}
	\caption{Beweis der kürzesten Pfade (selbst erstellt)}
	\label{fig:basic.proof-shortest-path-curve}
	\vspace{-24pt}
\end{wrapfigure}

\paragraph{Behauptung} Der kürzeste Weg zwischen zwei beliebigen Punkten, der die Hindernisse umgeht, ist ein Polygonzug, dessen Ecken entweder Knoten des Visibility Graph oder die Ausgangspunkte sind\cite{visgraph}.

\paragraph{Beweis} Die kürzeste Verbindung zwischen zwei beliebigen Punkten in der euklidischen Ebene ist eine gerade Linie.
Demzufolge sind alle Teilstücke eines kürzesten Pfads ebenfalls Geraden.
Angenommen, ein beliebiger Pfad $B$ enthalte einen Punkt $p$, der weder einer der Ausgangspunkte noch auf der Kante eines der Hindernisse sei.
Da $p$ Teil des freien Raums ist, gibt es einen kleinen Radius um $p$ herum, der ebenfalls vollständig frei von \mbox{Hindernissen} ist.
In diesem Radius kann der Pfad stets zu $B'$ gekürzt werden, indem eine Sehne \mbox{zwischen} dem Eingangs- und Ausgangspunkt von $B$ gespannt wird.
Die Sehne ist \mbox{eine} Gerade und damit lokal optimal.
Wird dies oft genug angewendet, konvergiert $B$ wie im roten Pfad gegen die Ecken der Hindernisse.

Dieser Beweis wurde auf Basis von \cite{vg-ileana} erbracht. Abbildung \ref{fig:basic.proof-shortest-path-curve} veranschaulicht diesen.

\begin{figure}[!ht]
	\centering
	\vspace{-10pt}
	\includegraphics[keepaspectratio,width=0.8\textwidth]{img/VG_shortest-path-example}
	\caption{Beispiel der kürzesten Pfade (selbst erstellt)}
	\label{fig:basic.proof-shortest-path}
	\vspace{-4pt}
\end{figure}

\paragraph{Beispiel} Übertragen auf das Beispiel in Abbildung \ref{fig:basic.proof-shortest-path} verläuft ein Pfad von $s$ über $m_x$ zu $e$.
Es existiert keine Sichtlinie zwischen $s$ und $e$, da das große Dreieck ein Hindernis ist.
Daher verläuft ein Pfad $sm_1e$ um das Dreieck herum.
Doch der Pfad kann hin zu $sm_2e$ und schließlich bis zu $sm_3e$ verkürzt werden.
Dabei konvergiert $m_x$ zum Eckpunkt des Hindernisses.

\paragraph{Zusammenfassung} Diese Technik ist sehr nützlich für viele Arten der Wegplanung als Teilgebiet der algorithmischen Geometrie (\emph{Computational Geometry}), da mit einem Visibility Graph die optimale Route von einem Startpunkt zu einem Endpunkt gefunden werden kann, solange der Algorithmus für die Wegsuche optimal ist.
